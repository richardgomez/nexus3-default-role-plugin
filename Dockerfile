FROM sonatype/nexus3

USER root

# Install the Plugin
COPY target/nexus-defaultrole-plugin-*.jar /opt/sonatype/nexus/system/nexus3-defaultrole-plugin.jar

RUN echo "reference\:file\:nexus3-defaultrole-plugin.jar = 200" >> /opt/sonatype/nexus/etc/karaf/startup.properties && \
    chown nexus:nexus -R /opt/sonatype/nexus

USER nexus
