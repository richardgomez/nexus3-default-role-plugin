package es.richardgom.nexus3.defaultrole.internal;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.junit.Before;
import org.junit.Test;
import org.sonatype.goodies.testsupport.TestSupport;
import org.sonatype.nexus.security.anonymous.AnonymousPrincipalCollection;

public class DefaultRoleRealmTest extends TestSupport {
    private DefaultRoleRealm realm;

    @Before
    public void setUp() {
        realm = new DefaultRoleRealm();
    }

    private static PrincipalCollection principals(final String userId) {
        if (userId.equals("anonymous")) {
            return new AnonymousPrincipalCollection(userId, "realm");
        }
        return new SimplePrincipalCollection(userId, "realm");
    }

    @Test
    public void unconfiguredPluginNoRoleGrantedTest() {
        realm.setRole(null);
        AuthorizationInfo info = realm.doGetAuthorizationInfo(principals("test"));
        assert info == null;
    }

    @Test
    public void authenticatedUserGrantedRoleTest() {
        realm.setRole("default-role");
        AuthorizationInfo info = realm.doGetAuthorizationInfo(principals("test"));
        assert info != null;
        assert info.getRoles().contains("default-role");
    }

    @Test
    public void anonymousUserNotGrantedRoleTest() {
        realm.setRole("default-role");
        AuthorizationInfo info = realm.doGetAuthorizationInfo(principals("anonymous"));
        assert info == null;
    }
}
