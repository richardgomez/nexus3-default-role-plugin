package es.richardgom.nexus3.defaultrole.internal;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.eclipse.sisu.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.nexus.security.anonymous.AnonymousPrincipalCollection;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Set;

/**
 * Provide a default role for authenticated users.
 * <p>
 * {@link AuthorizingRealm}
 *
 * @since 3.2
 */
@Named(DefaultRoleRealm.NAME)
@Singleton
@Description("Default Role Authorization Realm")
public class DefaultRoleRealm extends AuthorizingRealm {

    private static final Logger log = LoggerFactory.getLogger(DefaultRoleRealm.class);
    public static final String NAME = "DefaultRole";

    @Nullable
    private String role;

    @Nullable
    public String getRole() {
        return role;
    }

    public void setRole(@Nullable final String role) {
        this.role = role;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // Check that a default role has been set and the user isn't anonymous.
        if (role == null || principals instanceof AnonymousPrincipalCollection) {
            return null;
        }

        log.debug("Granting {} role: {}", principals, role);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRole(role);
        return info;
    }

    /**
     * @throws UnsupportedOperationException Authentication is not supported
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken authenticationToken) throws AuthenticationException {
        throw new UnsupportedOperationException();
    }
}
