package es.richardgom.nexus3.defaultrole.internal

import groovy.transform.PackageScope
import groovy.transform.ToString
import org.sonatype.goodies.i18n.I18N
import org.sonatype.goodies.i18n.MessageBundle
import org.sonatype.goodies.i18n.MessageBundle.DefaultMessage
import org.sonatype.nexus.capability.CapabilityConfigurationSupport
import org.sonatype.nexus.capability.CapabilityDescriptorSupport
import org.sonatype.nexus.capability.CapabilitySupport
import org.sonatype.nexus.capability.CapabilityType
import org.sonatype.nexus.capability.Capability
import org.sonatype.nexus.capability.Condition
import org.sonatype.nexus.capability.Tag
import org.sonatype.nexus.capability.Taggable
import org.sonatype.nexus.formfields.ComboboxFormField
import org.sonatype.nexus.formfields.FormField
import org.sonatype.nexus.security.realm.RealmManager

import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

import static org.sonatype.nexus.capability.CapabilityType.capabilityType

/**
 * {@link Capability} to configure the Default role.
 *
 * @since 3.2
 */
@Named(DefaultRoleCapability.TYPE_ID)
class DefaultRoleCapability extends CapabilitySupport<Configuration> {
    public static final String TYPE_ID = 'defaultrole'
    public static final CapabilityType TYPE = capabilityType(TYPE_ID)

    private static interface Messages extends MessageBundle {
        @DefaultMessage('Default Role')
        String name()

        @DefaultMessage('Security')
        String category()

        @DefaultMessage('Default Role')
        String roleLabel()

        @DefaultMessage('The role that is granted to all authenticated users')
        String roleHelp()

        @DefaultMessage('%s')
        String description(String role)

        @DefaultMessage('Disabled')
        String disabled()
    }

    @PackageScope
    static final Messages messages = I18N.create(Messages.class)

    @Inject
    RealmManager realmManager

    @Inject
    DefaultRoleRealm realm

    @Override
    protected Configuration createConfig(final Map<String, String> properties) {
        return new Configuration(properties)
    }

    @Override
    protected String renderDescription() {
        if (!context().active) {
            return messages.disabled()
        }
        return messages.description(config.role)
    }

    @Override
    Condition activationCondition() {
        return conditions().capabilities().passivateCapabilityDuringUpdate()
    }

    @Override
    protected void onActivate(final Configuration config) {
        realm.role = config.role
        realmManager.enableRealm(DefaultRoleRealm.NAME)
    }

    @Override
    protected void onPassivate(final Configuration config) {
        realm.role = null
        realmManager.disableRealm(DefaultRoleRealm.NAME)
    }

    //
    // Configuration
    //

    private static final String P_ROLE = 'role'

    @ToString(includePackage = false, includeNames = true)
    static class Configuration extends CapabilityConfigurationSupport {
        String role

        Configuration(final Map<String, String> properties) {
            role = parseUri(properties[P_ROLE])
        }
    }

    //
    // Descriptor
    //

    @Named(DefaultRoleCapability.TYPE_ID)
    @Singleton
    static class Descriptor extends CapabilityDescriptorSupport implements Taggable {
        private final FormField role

        Descriptor() {
            this.exposed = true
            this.hidden = false

            this.role = new ComboboxFormField<String>(
                    P_ROLE,
                    messages.roleLabel(),
                    messages.roleHelp(),
                    FormField.MANDATORY
            ).withStoreApi('coreui_Role.read')
        }

        @Override
        CapabilityType type() {
            return TYPE
        }

        @Override
        String name() {
            return messages.name()
        }

        @Override
        List<FormField> formFields() {
            return [role]
        }

        protected Configuration createConfig(final Map<String, String> properties) {
            return new Configuration(properties)
        }

        @Override
        protected String renderAbout() {
            // Hardcode the about value — render causes a NPE, for some reason.
            return '<p>Grant grant a default role to all <em>authenticated</em> users.</p>'

            // For some reason this causes a NPE
            // return render("$TYPE_ID-about.vm")
        }

        @Override
        Set<Tag> getTags() {
            return [Tag.categoryTag(messages.category())]
        }
    }
}
