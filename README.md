# Nexus3 Default Role Plugin

[![pipeline status](https://gitlab.com/richardgomez/nexus3-default-role-plugin/badges/master/pipeline.svg)](https://gitlab.com/richardgomez/nexus3-default-role-plugin/commits/master) [![coverage report](https://gitlab.com/richardgomez/nexus3-default-role-plugin/badges/master/coverage.svg)](https://gitlab.com/richardgomez/nexus3-default-role-plugin/commits/master)

This plugin adds the capability to assign a default role to authenticated users.

This is especially useful for LDAP authentication where users do not belong to a common group.

## Installation

**Note**: To avoid confusion, the Sonatype convention is used to reference directories: 
* nexus-3.x.x will be referred to as `$install-dir`
* sonatype-work/nexus3 will be referred to as `$data-dir`

See https://help.sonatype.com/repomanager3/installation/directories for reference.

**Prerequisites**
* JDK 8 is installed
* Sonatype Nexus 3.x is installed

**1. Download latest release jar into Nexus system folder**

Releases can be found here: https://gitlab.com/richardgomez/nexus3-default-role-plugin/-/packages
```bash
$ cd $install-dir/system/
$ wget -O nexus3-defaultrole-plugin.jar https://gitlab.com/richardgomez/nexus3-default-role-plugin/-/package_files/243089/download
```

**2. Add bundle to startup properties**

Append the following line to the `startup.properties` file found in `$install-dir/etc/karaf`
```bash
$ echo "reference\:file\:nexus3-defaultrole-plugin.jar = 200" >> $install-dir/etc/karaf/startup.properties
```

**3. Restart Nexus**

Restart your Nexus service for the changes to take effect.

## Usage

After installation you must enable the plugin:
 
 * Navigate to: *Administration* > *System* > *Capabilities*
 * Click: *Create capability*
 * Select: *Default Role*
 * Choose a default role and click: *Create capability*
 
By default the only roles available are `nx-anonymous` and `nx-admin`. If you do not wish to use those you can [create a new role](https://help.sonatype.com/repomanager3/security/roles) (e.g. `nx-authenticated`) and select that instead.

## Development

You can build the project with the integrated maven wrapper like so: `./mvnw clean package`

#### Test in a Docker Nexus instance

To test locally you can use the provided [`Dockerfile`](Dockerfile), which installs the plugin from your local [`target/`](target) directory.

* Build the plugin:
```bash
$ ./mvnw clean package
```
* Build the image: 
```bash
$ docker build -t nxrm-defaultrole-dev .
```
* Run the image: 
```bash
$ docker run -it -p 8081:8081 nxrm-defaultrole-dev
```
* (From a separate terminal) Retrieve the admin password for later use:
```bash
$ docker ps
CONTAINER ID     IMAGE                  ...
<container-id>   nxrm-defaultrole-dev   ...
$ docker exec -it <container-id> cat /nexus-data/admin.password
220b2906-eg1e-4cf6-821c-abd7eee6f3dc
```

You can now log in to Nexus from your browser at http://localhost:8081 with the username `admin` and the password retrieved above (e.g. `220b2906-eg1e-4cf6-821c-abd7eee6f3dc`).

**If you do not have an LDAP server** you can use a mock instance, like the [ldap-server-mock](https://github.com/docker-ThoTeam/ldap-server-mock), for testing.

## Credits

This plugin is a fork of the [Nexus AutoRole Plugin](https://github.com/sonatype/nexus-public/pull/21) created by [Jason Dillon](https://github.com/jdillon).
 
The following projects were used for inspiration and reference:
- [Nexus3 Crowd Plugin](https://github.com/pingunaut/nexus3-crowd-plugin)
- [Nexus Okta Auth Plugin](https://github.com/ruhkopf/nexus-okta-auth-plugin)
- [Nexus3 GitHub OAuth Plugin](https://github.com/larscheid-schmitzhermes/nexus3-github-oauth-plugin)
